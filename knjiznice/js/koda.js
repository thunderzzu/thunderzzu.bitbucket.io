var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#imePacienta").val();
	var priimek = $("#priimekPacienta").val();
    var datumRojstva = $("#datumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#responseKreiraj").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#responseKreiraj").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#responseKreiraj").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function getPulse() {
    $("#responsePridobi").html("");
    
    var sessionId = getSessionId();

	var ehrId = $("#ehrIDPridobi").val();
	
	        $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#responsePridobiIme").append("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#responsePridobiIme").append("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});

            $.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "pulse",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table " +
                    "table-hover'><thead class='thead-dark'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Utrip/min</th></tr></thead>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].pulse +
                          " " + res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#responsePridobi").append(results);
					    	} else {
					    		$("#responsePridobi").append(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#responsePridobi").append(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
}

function vnesiPodatkeEHR() {
    var sessionId = getSessionId();

	var ehrId = $("#vnesiID").val();
	var datumInUra = $("#vnesiDatum").val();
	var utrip = $("#vnesiUtrip").val();
	var merilec = "jst";
	
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/pulse/any_event/rate|magnitude": utrip,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#responseVnesi").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Vnos meritve je bil uspešno dodan" + ".</span>");
		    },
		    error: function(err) {
		    	$("#responseVnesi").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
    
}

function algoritem() {
    var sessionId = getSessionId();
	var ehrId = $("#vnesiIdAlgoritem").val();
	console.log(2.5/10);
	
	$.ajax({
  	    url: baseUrl + "/view/" + ehrId + "/" + "pulse",
	    type: 'GET',
	    headers: {"Ehr-Session": sessionId},
	    success: function (res) {
	        if (res.length >= 1) {
	            var testctr = 0;
	            for (var i = 0; i<res.length; i++) {
	                if (res[i].pulse != 0) {
	                    testctr++;
	                }
	            }
	            if (testctr == res.length) {
	                $("#responseAlgoritem").html("<div class='alert alert-success' role='alert'>Zdravi ste kot riba!</div>");
	                $("#responseNews").html("");
	            } else {
	            if (res.length >= 2 && res.length <= 5) {
	                if (res[0].pulse == 0 && res[1].pulse != 0) {
	                    $("#responseAlgoritem").html("<div class='alert alert-warning' role='alert'>Če imate srečo, se merilec moti.</div>");
	                    showNews();
	                } else if (res[0].pulse == 0 && res[1].pulse == 0) {
	                    $("#responseAlgoritem").html("<div class='alert alert-dark' role='alert'>Definitivno niste več živi. Posvetujte se s farmacevtom ali z zasebnim zdravnikom.</div>");
	                    showNews();
	                } else {
	                    $("#responseAlgoritem").html("<div class='alert alert-primary' role='alert'>Recimo, da je pri meritvah bilo nekaj napak in da ste v bistvu zelo zdravi. Čestitke! </div>");
	                }
	            } else if (res.length > 5 && res.length <= 10) {
	                var cntr = 0;
	                for (var i = 0; i<res.length; i++) {
	                    if (res[i].pulse == 0) {
	                        cntr++;
	                    }
	                }
	                if (cntr >= 5) {
	                    $("#responseAlgoritem").html("<div class='alert alert-dark' role='alert'>Definitivno niste več živi. Posvetujte se s farmacevtom ali z zasebnim zdravnikom.</div>");
	                    showNews();
	                } else {
	                    $("#responseAlgoritem").html("<div class='alert alert-primary' role='alert'>Mogoče še imate šanse. Vendar nič ne obljubimo.</div>");
	                }
	            } else if (res.length == 1) {
	                if (res[0].pulse == 0) {
	                    $("#responseAlgoritem").html("<div class='alert alert-warning' role='alert'>Premalo meritev, da bi zagotovo rekli, ampak zaenkrat ne kaže dobro.</div>");
	                    showNews();
	                } else {
	                    $("#responseAlgoritem").html("<div class='alert alert-success' role='alert'>Zdravi ste kot riba!</div>");
	                    $("#responseNews").html("");
	                }
	            } else if (res.length > 10) {
	                var cntr2 = 0;
	                
	                for (var i = 0; i<res.length; i++) {
	                    if (res[i].pulse == 0) {
	                        cntr2++;
	                    }
	                }
	                if (cntr2/res.length > 0.25) {
	                    $("#responseAlgoritem").html("<div class='alert alert-dark' role='alert'>Izgledate precej mrtvi. Predlagamo vam obisk zdravnika. Ali pa pogrebnega zavoda.</div>");
	                    showNews();
	                } else {
	                    $("#responseAlgoritem").html("<div class='alert alert-primary' role='alert'>Mogoče še imate šanse. Vendar nič ne obljubimo.</div>");
	                    showNews();
	                }
	            }
	            
	            }
	            
            } else {
            $("#responseAlgoritem").html(
          "<div class='alert alert-danger' role='alert'>" +
          "Algoritem ne more delovati brez vnosov meritev!" + "</div>");
            }
	    },
	    error: function() {
	    	$("#responseAlgoritem").append(
  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
  JSON.parse(err.responseText).userMessage + "'!");
	    }
	});
}

function showNews() {
    var xmlhttp = new XMLHttpRequest();
    var url = 'https://newsapi.org/v2/everything?sources=medical-news-today&q=heart%20attack&apiKey=d484b98295a24e99b3a39d868b907e7e';
    
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            $("#responseNews").html("");
            var results = "<table class='table table-striped'><thead><tr><th scope='col'>Ime članka</th><th>Opis članka</th></tr></thead><tbody>";
            
            if (myArr.articles.length > 5) {
                for (var i = 0; i < 5; i++) {
                    results += "<tr><td><a href='" + myArr.articles[i].url + "'>" + myArr.articles[i].title + "</a></td><td>" + myArr.articles[i].description + "</td></tr>";
                }
            } else {
                for (var i = 0; i<myArr.articles.length; i++) {
                    results += "<tr><td>" + myArr.articles[1].title + "</td><td>" + myArr.articles[1].description + "</td></tr>";
                }
            }
            
            results += "</tbody></table>";
            $("#responseNews").html(results);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function showChoice(str) {
    if (str == 1) {
        document.getElementById("pridobiPodatke").style.display= "none";
        document.getElementById("vnesiMeritve").style.display= "none";
        document.getElementById("areYouDead").style.display= "none";
        document.getElementById("kreirajPacienta").style.display = "block";
    } else if(str == 2) {
        document.getElementById("vnesiMeritve").style.display= "none";
        document.getElementById("areYouDead").style.display= "none";
        document.getElementById("kreirajPacienta").style.display = "none";
        document.getElementById("pridobiPodatke").style.display= "block";
    } else if(str == 3) {
        document.getElementById("areYouDead").style.display= "none";
        document.getElementById("kreirajPacienta").style.display = "none";
        document.getElementById("pridobiPodatke").style.display= "none";
        document.getElementById("vnesiMeritve").style.display= "block";

    } else if(str == 4) {
        document.getElementById("kreirajPacienta").style.display = "none";
        document.getElementById("pridobiPodatke").style.display= "none";
        document.getElementById("vnesiMeritve").style.display= "none";
        document.getElementById("areYouDead").style.display= "block";
    }
}

function generirajPodatke() {
	$("#podatki").html(" ");
	generirajPodatke1();
	generirajPodatke2();
	generirajPodatke3();

}

function generirajPodatke1() {
	var ime = "Janez";
	var priimek = "Pomorski";
	var datum = "1999-12-12";
	var sessionId = getSessionId();
	
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#podatki").append("<a href='#' onclick='vstavi1()'><p class='podatkiEHR1'>" + ime + " " + priimek + ", " + datum + ", " + ehrId + "</p></a>");
		                }
						var datumInUra = "2015-10-10";
						var utrip = 0;
						var merilec = "jst";
						
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra,
							    "vital_signs/pulse/any_event/rate|magnitude": utrip,
							};
							var parametriZahteve = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
						var datumInUra2 = "2015-10-11";
						var utrip2 = 0;
						var merilec2 = "jst";
						
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki2 = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra2,
							    "vital_signs/pulse/any_event/rate|magnitude": utrip2,
							};
							var parametriZahteve2 = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec2
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve2),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki2),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
							
		            },
		            error: function(err) {
		            ;
		            }
		        });
		    }
		});
}

function generirajPodatke2() {
	var ime = "Vesela";
	var priimek = "Marela";
	var datum = "2000-10-11";
	var sessionId = getSessionId();
	
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#podatki").append("<a href='#' onclick='vstavi2()'><p class='podatkiEHR2'>" + ime + " " + priimek + ", " + datum + ", " + ehrId + "</p></a>");
		                }
						var datumInUra = "2015-10-10";
						var utrip = 75;
						var merilec = "jst";
						
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra,
							    "vital_signs/pulse/any_event/rate|magnitude": utrip,
							};
							var parametriZahteve = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
						var datumInUra2 = "2015-10-13";
						var utrip2 = 62;
						var merilec2 = "jst";
						
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki2 = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra2,
							    "vital_signs/pulse/any_event/rate|magnitude": utrip2,
							};
							var parametriZahteve2 = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec2
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve2),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki2),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
							
		            },
		            error: function(err) {
		            ;
		            }
		        });
		    }
		});
}

function generirajPodatke3() {
	var ime = "Pamela";
	var priimek = "Anderson";
	var datum = "1729-06-05";
	var sessionId = getSessionId();
	
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#podatki").append("<a href='#' onclick='vstavi3()'><p class='podatkiEHR3'>" + ime + " " + priimek + ", " + datum + ", " + ehrId + "</p></a>");
		                }
						var datumInUra = "2016-10-10";
						var utrip = 0;
						var merilec = "jst";
						
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra,
							    "vital_signs/pulse/any_event/rate|magnitude": utrip,
							};
							var parametriZahteve = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
						var datumInUra2 = "2017-10-13";
						var utrip2 = 75;
						var merilec2 = "jst";
						
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki2 = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumInUra2,
							    "vital_signs/pulse/any_event/rate|magnitude": utrip2,
							};
							var parametriZahteve2 = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec2
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve2),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki2),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
							
		            },
		            error: function(err) {
		            ;
		            }
		        });
		    }
		});
}

function vstavi1() {
	var ime="Janez";
	var priimek="Pomorski";
	var datum="1999-12-12";
	var id = document.getElementsByClassName("podatkiEHR1")[0].innerHTML.split(" ")[3];
	document.getElementById("imePacienta").value = ime;
	document.getElementById("priimekPacienta").value = priimek;
	document.getElementById("datumRojstva").value = datum;
	document.getElementById("ehrIDPridobi").value = id;
	document.getElementById("vnesiID").value = id;
	document.getElementById("vnesiIdAlgoritem").value = id;
}

function vstavi2() {
	var ime="Vesela";
	var priimek="Marela";
	var datum="2000-10-11";
	var id = document.getElementsByClassName("podatkiEHR2")[0].innerHTML.split(" ")[3];
	document.getElementById("imePacienta").value = ime;
	document.getElementById("priimekPacienta").value = priimek;
	document.getElementById("datumRojstva").value = datum;
	document.getElementById("ehrIDPridobi").value = id;
	document.getElementById("vnesiID").value = id;
	document.getElementById("vnesiIdAlgoritem").value = id;
}

function vstavi3() {
	var ime="Pamela";
	var priimek="Anderson";
	var datum="1729-06-05";
	var id = document.getElementsByClassName("podatkiEHR3")[0].innerHTML.split(" ")[3];
	document.getElementById("imePacienta").value = ime;
	document.getElementById("priimekPacienta").value = priimek;
	document.getElementById("datumRojstva").value = datum;
	document.getElementById("ehrIDPridobi").value = id;
	document.getElementById("vnesiID").value = id;
	document.getElementById("vnesiIdAlgoritem").value = id;
}